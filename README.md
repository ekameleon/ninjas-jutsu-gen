# Gen prototype #
    
## DESCRIPTION ##
    
A pure ActionScript 3 prototype (test).
    
## VERSION ##
    
1.0
    
## LICENCE ## 
    
Mozilla Public License 1.1 (MPL 1.1) (read Licence.txt)
    
## PROJECT PAGES ##
    
 * https://bitbucket.org/ekameleon/ninjas-hyoton-flash
 * https://bitbucket.org/ekameleon/vegas (dependency)
    
## ABOUT ##
    
 * Author : ALCARAZ Marc (eKameleon)
 * Link   : http://www.ooopener.com
 * Mail   : ekameleon@gmail.com

## THANKS ##

FDT, the best ActionScript editor Forever !

![FDT_Supported.png](https://bitbucket.org/repo/AEbB9b/images/525527175-FDT_Supported.png) 
    
 * PowerFlasher - FDT OpenSource Licence : http://fdt.powerflasher.com/