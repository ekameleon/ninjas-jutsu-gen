﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.gen.media
{
    import graphics.colors.ColorMatrix;

    import flash.display.BitmapData;
    import flash.display.BlendMode;
    import flash.filters.BlurFilter;
    import flash.filters.ColorMatrixFilter;
    import flash.geom.Matrix;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.media.Video;
    
    /**
     * Basic motion tracking who using webcam as imput. 
     */
    public class MotionTracker extends Point
    {
        /**
         * Creates a new MotionTracker instance.
         * @param source The Video reference to track motion.
         */
        public function MotionTracker( source:Video )
        {
            _blurFilter  = new BlurFilter(20,20) ;
            _colorMatrix = new ColorMatrix() ;
            
            input = source ;
        }
        
        /**
         * Indicates if a movement is currently being detected.
         */
        public function get activity():Boolean 
        { 
            return _activity; 
        }
        
        /**
         * The blur applied to the input video in order to improve accuracy.
         */
        public function get blur():int 
        { 
            return _blurFilter.blurX; 
        }
        
        /**
         * @private
         */
        public function set blur( value:int ):void 
        { 
            _blurFilter.blurX = _blurFilter.blurY = value ; 
        }
        
        /**
         * The brightness being applied to the input.
         */
        public function get brightness():int 
        { 
            return _colorMatrix.brightness ; 
        }
        
        /**
         * @private
         */
        public function set brightness( value:int ):void
        {
            _brightness = value;
            _update();
        }
        
        /**
         * The contrast being applied to the input.
         */
        public function get contrast():int 
        { 
            return _colorMatrix.contrast ; 
        }
        
        /**
         * @private
         */
        public function set contrast( value:int ):void
        {
            _contrast = value;
            _update();
        }
        
        /**
         * The Video reference used to track motion.
         */
        public function get input():Video 
        { 
            return _input; 
        }
        
        /**
         * @private
         */
        public function set input( video:Video ):void
        {
            _input = video ;
            if ( _now ) 
            { 
                _now.dispose() ; 
            }
            if( _old )
            {
                _old.dispose() ;
            }
            _now = new BitmapData(video.width, video.height, false, 0);
            _old = new BitmapData(video.width, video.height, false, 0);
        }
        
        /**
         * The minimum area (percent of the input dimensions) of movement to be considered movement.
         */
        public function get minArea() : uint 
        { 
            return _minArea; 
        }
        
        /**
         * @private
         */
        public function set minArea( value:uint ):void
        {
            _minArea = value ;
        }
        
        /**
         * The area in which movement is being detected.
         */
        public function get motionArea():Rectangle 
        { 
            return _box; 
        }
        
        /**
         * The current area of the image the tracker is working from.
         */
        public function get trackingArea() : Rectangle 
        { 
            return new Rectangle( _input.x , _input.y , _input.width , _input.height ); 
        }
        
        /**
         * The current image of the tracker is working from.
         */
        public function get trackingImage():BitmapData 
        { 
            return _now ; 
        }
        
        /**
         * Track movement within the source Video object.
         */
        public function run( ...arguments:Array ):void
        {
            if( _input )
            {
                _now.draw(_input , _matrix );
                _now.draw( _old  , null, null, BlendMode.DIFFERENCE ) ;
                
                _now.applyFilter( _now , _now.rect , new Point() , _colorMatrixFilter );
                _now.applyFilter( _now , _now.rect , new Point() , _blurFilter );
                
                _now.threshold(_now, _now.rect, new Point(), '>', 0xFF333333, 0xFFFFFFFF);
                _old.draw( _input , _matrix ) ;
                
                var area:Rectangle = _now.getColorBoundsRect(0xFFFFFFFF, 0xFFFFFFFF, true) ;
                
                _activity = ( area.width > ( _input.width / 100 ) * _minArea ) || ( area.height > ( _input.height / 100 ) * _minArea ) ;
                
                if ( _activity )
                {
                    _box = area ;
                    x = _box.x + (_box.width / 2);
                    y = _box.y + (_box.width / 2);
                }
            }
        }
        
        /**
         * @private
         */
        private var _activity:Boolean;
        
        /**
         * @private
         */
        private var _box:Rectangle ;
        
        /**
         * @private
         */
        private var _blurFilter:BlurFilter ;
        
        /**
         * @private
         */
        private var _brightness:Number = 0;
        
        /**
         * @private
         */
        private var _colorMatrix:ColorMatrix ;
        
        /**
         * @private
         */
        private var _colorMatrixFilter:ColorMatrixFilter ;
        
        /**
         * @private
         */
        private var _contrast:Number = 0;
        
        /**
         * @private
         */
        private var _input:Video;
        
        /**
         * @private
         */
        private var _matrix:Matrix;
        
        /**
         * @private
         */
        private var _minArea:uint = 10;
        
        /**
         * @private
         */
        private var _now:BitmapData;
        
        /**
         * @private
         */
        private var _old:BitmapData;
        
        /**
         * @private
         */
        private function _update():void
        {
            _colorMatrix.reset();
            _colorMatrix.contrast   = _contrast ;
            _colorMatrix.brightness = _brightness ;
            _colorMatrixFilter      = new ColorMatrixFilter( _colorMatrix.matrix );
        }
    }
}
