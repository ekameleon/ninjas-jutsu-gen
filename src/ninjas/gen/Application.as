﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.gen
{
    import core.reflect.getClassName;

    import graphics.FillStyle;
    import graphics.LineStyle;

    import ninjas.gen.display.Body;
    import ninjas.gen.display.Wallpaper;
    import ninjas.gen.logging.sos;

    import system.logging.LoggerLevel;
    import system.logging.targets.SOSTarget;

    import vegas.display.Root;

    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    
    [SWF(width="760", height="480", frameRate="24", backgroundColor="#A2A2A2")]
    // -default-size 740 480 -default-frame-rate 24 -default-background-color 0x333333 -target-player=10.0 -optimize=true
    
    /**
     * The main class of the Gen Application.
     */
    public class Application extends Root
    {
        /**
         * Creates a new Application instance.
         */
        public function Application()
        {
            super();
        }
        
        /**
         * Invoked when the display is added to the stage.
         */
        protected override function addedToStage( e:Event = null ):void
        {
            // stage
            
            stage.align                  = StageAlign.TOP_LEFT ; 
            stage.scaleMode              = StageScaleMode.NO_SCALE ;
            stage.showDefaultContextMenu = false ;
            
            // logging
            
            var noDebug : * = flashVars.getValue("debug") === "false" ;
            if ( !noDebug && !sos ) 
            {  
                sos = new SOSTarget( getClassName(this) , 0xA2A2A2 ) ;
                
                sos.filters       = ["*"] ;
                sos.level         = LoggerLevel.ALL ;
                sos.includeLines  = true  ;
                sos.includeTime   = true  ;
            }
            
            initialize() ;
        }
        
        /**
         * Initialize the application.
         */
        protected function initialize():void
        {
            logger.log( this + " initialize" ) ;
            
            // initialize the object factory
            
            factory.config.parameters = flashVars ;
            factory.config.root       = this ;
            factory.config.stage      = stage ;
            
            factory.create( definitions ) ;
        }
        
        //////////////// Linkage Enforcer
        
        // graphics
        
        LineStyle ; FillStyle ;
        
        // gen
        
        Body ; Wallpaper ;
        
        ////////////////
    }
}
